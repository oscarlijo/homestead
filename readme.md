<p align="center"><img src="https://laravel.com/assets/img/components/logo-homestead.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/homestead"><img src="https://travis-ci.org/laravel/homestead.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/homestead"><img src="https://poser.pugx.org/laravel/homestead/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/homestead"><img src="https://poser.pugx.org/laravel/homestead/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/homestead"><img src="https://poser.pugx.org/laravel/homestead/license.svg" alt="License"></a>
</p>

## Requisitos

Antes de nada tenemos que tener instalado en nuestro equipo VirtualBox y Vagrant

https://www.virtualbox.org/wiki/Downloads

https://www.vagrantup.com/downloads.html

## Instalación

En primer lugar necesitamos el Box de Homestead, con el que por defecto tenemos esto:

    Ubuntu 16.04
    Git
    PHP 7.1
    PHP 7.0
    PHP 5.6
    Nginx
    MySQL
    MariaDB
    Sqlite3
    PostgreSQL
    Composer
    Node (With Yarn, Bower, Grunt, and Gulp)
    Redis
    Memcached
    Beanstalkd
    Mailhog
    ngrok

Para instalar el box solo necesitamos ejecutar este comando.

<code>vagrant box add laravel/homestead</code>

Ahora necesitamos instalar nuestro proyecto el cual podemos descargar de este repositorio ejecutando el siguiente comando:

git clone https://gitlab.com/oscarlijo/homestead.git


## Configuración

Está configurado para mi máquina por lo que las rutas pueden ser distintas en tu caso, solo tienes que comprobarlo en el fichero **Homestead.yaml**, en la linea **13**, yo por defecto lo tengo en *C:\wamp64\www\learningmaker*

También hay que tener presente el nombre de la base de datos a la que apunta el proyecto, en mi caso se llama *learningmaker*, si tu le has puesto otro nombre lo puedes modificar también en el fichero **Homestead.yaml**, en la linea **21**

Ahora vamos a configurar el acceso por ssh necesario en caso de que tengamos que hacer alguno de los pasos que explicaremos a continuación, para hacerlo necesitamos generar unas claves que ssh usará para encriptar la comunicación y hacerla segura, para ello solamente tenemos que ejecutar **desde la consola de git** este comando:

<code>ssh-keygen -t rsa -C "tu@email.com"</code>

Como acabamos de instalar MySQL solo tendremos un usuario 'root' sin contraseña, si en nuestro proyecto establecimos una contraseña podemos quitársela, o sino añadirle a root la contraseña en cuestión.
Para eso tenemos que seguir tres sencillos pasos.
1. Conectarnos por ssh con el comando <code>vagrant ssh</code>
2. Abrir el terminal de MySQL con el comando <code>mysql</code>
3. Cambiar la contraseña del root con el comando <code>SET PASSWORD FOR 'root'@'localhost' = 'tu_contraseña';</code>

Ahora solo tenemos que poblar la base de datos, para ello entramos dentro del proyecto desde ssh de esta manera.
1. Conectarnos por ssh con el comando <code>vagrant ssh</code>
2. Entramos en el proyecto con el comando <code>cd learningmaker</code>
3. Poblamos la BBDD con el comando <code>php index.php default installation</code>


## Extras

Algo muy interesante que no trae Homestead por defecto es la herramienta PHPMyAdmin, pero la podemos instalar sin problemas siguiendo estos pasos.

1. Conectarnos por ssh con el comando <code>vagrant ssh</code>
2. Entramos en el proyecto con el comando <code>cd learningmaker</code>
3. Ejecuta estos comandos.
<code>curl -#L https://files.phpmyadmin.net/phpMyAdmin/4.7.5/phpMyAdmin-4.7.5-all-languages.tar.gz -o phpmyadmin.tar.gz</code>
<code>mkdir phpmyadmin && tar xf phpmyadmin.tar.gz -C phpmyadmin --strip-components 1</code>
<code>rm phpmyadmin.tar.gz</code>

*NOTA: Cuando todas las dependencias de Maker esten definidas llevaremos todo esto a un script de aprovisionamiento*

## Puesta en marcha

Hecho todo esto hay que abrir un terminal en la carpeta de Homestead y escribir

<code>vagrant up</code>

La máquina se habrá levantado y podremos verla funcionando en la siguiente dirección:

http://192.168.10.10/

Como llamarlo de esta manera es poco descriptivo vamos a definir un nuevo host en Windows

Tenemos que ir a **C:\Windows\System32\drivers\etc\hosts**

Y al final del archivo añadimos esta linea.

<code>192.168.10.10  learningmaker.app</code>

Ahora ya podemos acceder a nuestra instancia de Maker con la dirección:

http://learningmaker.app/


## Créditos

Laravel Homestead is an official, pre-packaged Vagrant box that provides you a wonderful development environment without requiring you to install PHP, a web server, and any other server software on your local machine. No more worrying about messing up your operating system! Vagrant boxes are completely disposable. If something goes wrong, you can destroy and re-create the box in minutes!

Homestead runs on any Windows, Mac, or Linux system, and includes the Nginx web server, PHP 7.1, MySQL, Postgres, Redis, Memcached, Node, and all of the other goodies you need to develop amazing Laravel applications.

Official documentation [is located here](https://laravel.com/docs/homestead).
